## Quick start

Pour lancer rapidement le code veuillez créer deux terminaux :

-exécutez ce code:

#### installer les dépendances
```
pip install -r requirements.txt

```
#### Créer Un index 
- l'index est au format {"token1": {"doc_k": {"position": [i], "count": j},{{"doc_h": {"position": [p], "count ": n}}}
étapes pour créer l'index :
- extraire le titre faire le traitement nécessaire (supprimer les caractères spéciaux,l'unicode et supprimer les stopswords)
- Tokenser
- créer enfin un index avec les tokens leurs positions et leurs occurrences.


Pour la stemmatisation :

   - -  J'ai testé les différents modules d'API nltk stemm pour la stemmatisation et j'ai écrit les résultats dans le fichier "test/teststemIndex.txt". J'ai choisi "stemmer_snowball()" qui, selon les tests que j'ai effectués, s'est avéré être plus efficace pour le français(la plupart des tokens sont en français). Cependant, ce point devra être développé davantage car il est important de préciser la langue avant de faire le processus de stemmatisation.
   - -  J'ai également essayé avec la bibliothèque python "langdetect", mais les résultats obtenus n'étaient pas pertinents. Il faut donc implémenter une partie pour gérer le problème de la langue avant d'effectuer la stemmatisation.

#### Pour lancer:

```
python3 main.py 
```
#### Pour tester:
```
python -m unittest 
```