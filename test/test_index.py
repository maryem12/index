import unittest
from indexminimal.index import creer_index
class TestIndex(unittest.TestCase):
    def test_creer_index(self):
        url=["https://fr.wikipedia.org/wiki/Karine_Lacombe"]
        index,document_count, token_count=creer_index(url)
        self.assertEqual(index,{'karine': {1: {'position': [0], 'count': 1}},'lacombe': {1: {'position': [1], 'count': 1}},'wikipedia': {1: {'position': [2], 'count': 1}}})
        self.assertEqual(document_count,1)
        self.assertEqual(token_count,3)
        




