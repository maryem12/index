import nltk
import unittest
from indexminimal.stemIndex import *
class TestIndex(unittest.TestCase):
    # Définir la phrase à traiter
    phrase = "Les chats et les chiens sont des animaux domestiques."

    # Tokenizer la phrase
    tokens = nltk.word_tokenize(phrase)
    def test_stemmer_isri(self):
        stemmed_tokens_isri=[stemmer_isri(token) for token in self.tokens]
        print("stemmer_isri-----",stemmed_tokens_isri)
    def test_stemmer_lancaster(self):
        stemmed_tokens_lancaster=[stemmer_lancaster(token) for token in self.tokens]
        print("stemmer_lancaster-----",stemmed_tokens_lancaster)
    
    def test_stemmer_porter(self):
        stemmed_tokens_porter=[stemmer_porter(token) for token in self.tokens]
        print("stemmer_porter-----",stemmed_tokens_porter)
       
    def test_stemmer_regexp(self):
        stemmed_tokens_regexp=[stemmer_regexp(token) for token in self.tokens]
        print("stemmer_regexp-----",stemmed_tokens_regexp)
        
    def test_stemmer_snowball(self):
        stemmed_tokens_snowball=[stemmer_snowball(token) for token in self.tokens]
        print("stemmer_snowball-----",stemmed_tokens_snowball)
        
