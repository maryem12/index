import unittest
from indexminimal.statistiques import freq_token,extract_top_token
class TestStatistiques(unittest.TestCase):
    def test_freq_token(self):
        d={'bonjour': {1: {'position': [0], 'count': 2},
        2: {'position': [0], 'count': 1}},
        'vie': {1: {'position': [1], 'count': 5}},
        'cv': {1: {'position': [2], 'count': 1}, 2: {'position': [1], 'count': 3}},
        'france': {2: {'position': [2], 'count': 1}},
        'elecotronique': {3: {'position': [0], 'count': 1}}}
        
        sorted_d=freq_token(d)
        self.assertEqual(sorted_d,{'bonjour': 3, 'vie': 5, 'cv': 4, 'france': 1, 'elecotronique': 1})
    def test_extract_top_token(self):
        #le dictionnaire de fréquence
        d={'bonjour': 3, 'vie': 5, 'cv': 4, 'france': 1, 'elecotronique': 1}
        top2=extract_top_token(d,2)
        self.assertEqual(top2,{'vie': 5, 'cv': 4})
        

        



        
