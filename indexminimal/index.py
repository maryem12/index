import pandas as pd 
from nltk.tokenize import word_tokenize
import time
from indexminimal.utils import preprocessing,stopWords,extraire_title
def creer_index(urls):
    # Initialiser l'index
    index={}
    # Initialiser un compteur pour les documents
    document_count=0
    # Initialiser un compteur pour les tokens
    token_count=0
    for url in urls:
        title =extraire_title(url,s=5)
        if title !=False:
             #certaines utls ne marchent pas
            cleaned_title=preprocessing(title)            
            # Tokenizer le contenu
            tokens = word_tokenize(cleaned_title)
            filtered_tokens =stopWords(tokens)

             #Incrémenter les compteurs de documents et de tokens
            document_count += 1
            token_count += len(filtered_tokens)

                # Boucle sur chaque token
            for i, token in enumerate(filtered_tokens): #i pour la position
                    # Si le token n'est pas déjà dans l'index, l'ajouter avec un dictionnaire vide
                if token not in index:
                    index[token] = {}
                    # Si l'url n'est pas dans l'index ajouter un dictionnaire vide
                if url not in index[token]:
                    index[token][document_count] = {'position':[i],'count':1}
                else:
                    index[token][document_count]['position'].append(i)
                    index[token][document_count]['count'] += 1
            # attendre for 2 seconds
                time.sleep(3)
        else:
            continue

           
    return index,document_count, token_count