from collections import Counter
def avg_tokens_par_doc(num_tokens,num_docs):
    avg_tokens_per_doc = num_tokens / num_docs
    return avg_tokens_per_doc



def freq_token(index):
    #fonction qui calcul la frequence de chaque token dans tout l'index
    freq={}
    for token in index.keys():
        s=0
        for i in index[token].keys():

            s+=index[token][i]['count']
            freq[token]=s
    return freq #dic qui contient la frequence de chaque token
def extract_top_token(freq,k):
    #fonction qui trie dic de fréquence et renvoi le top-k token s
    sorted_freq= dict(sorted(freq.items(), key=lambda item: item[1], reverse=True))
    top_k =dict(Counter(sorted_freq).most_common(k))
    return top_k 

