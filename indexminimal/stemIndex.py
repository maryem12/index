import nltk
from nltk.stem import *

# les différentes méthodes de stemmatisation
def stemmer_isri(token):
    stemmer_isri = nltk.stem.isri.ISRIStemmer()
    stemmed_token_isri =stemmer_isri.stem(token)
    return stemmed_token_isri
def stemmer_lancaster(token):
    stemmer_lancaster = nltk.stem.lancaster.LancasterStemmer()
    stemmed_token_lancaster =stemmer_lancaster.stem(token)
    return stemmed_token_lancaster
def stemmer_porter(token):
    stemmer_porter = nltk.stem.porter.PorterStemmer()
    stemmed_token_porter =stemmer_porter.stem(token)
    return stemmed_token_porter
def stemmer_regexp(token):
    stemmer_regexp = nltk.stem.regexp.RegexpStemmer('[^a-zA-Z0-9]+', min=4)
    stemmed_token_regexp =stemmer_regexp.stem(token)
    return stemmed_token_regexp 

def stemmer_snowball(token):
    stemmer_snowball = nltk.stem.snowball.FrenchStemmer()
    stemmed_token_snowball =stemmer_snowball.stem(token)
    return  stemmed_token_snowball

