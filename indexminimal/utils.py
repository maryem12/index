import re
from unidecode import unidecode
from nltk.corpus import stopwords
import requests
from bs4 import BeautifulSoup
import json
from Stemmer import Stemmer
def preprocessing(text):
    text=re.sub(r"[^a-zA-Z0-9\w]"," ",unidecode(text.lower()))
    return text
def stem_tokens(token):
    #ici on utliser un stemmer french car l plupart des urls sont en francais 
    #'jai tester la biblo python pour detecter la langue mais ca marche pas avec seulement les titres 
    #a devlopper :use  stopswords aussi detecter si on "en" "fr" dans url..
    stemmer = Stemmer("french")
    stemmed_tokens =stemmer.stemWord(token)
    return stemmed_tokens
def stopWords(tokens):
    stop_words = set(stopwords.words())
    filtered_tokens = [token for token in tokens if token.lower() not in stop_words]
    return filtered_tokens
def extraire_title(url,s):#s un délai d'attente maximal pour la réception de la réponse
    try:
        
        page=requests.get(url,timeout=s)
        soup=BeautifulSoup(page.content, "html.parser")
        title =soup.find('title').get_text()
        return title
    except:
        return False
def update_json(keys,old_json):
    #cette fonction sert à affecter les valeurs de l'index  non stemmer à l'index stemmer
    new_json= {keys[i]: value for i, value in enumerate(old_json.values())}
    return new_json
