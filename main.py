
from indexminimal.index import creer_index
from indexminimal.statistiques import avg_tokens_par_doc ,freq_token,extract_top_token
from indexminimal.stemIndex import stemmer_snowball
from indexminimal.utils import update_json
import json

def run():
    with open('crawled_urls.json', 'r') as f:
        urls =json.load(f)
    index,document_count,token_count=creer_index(urls[0:3])
    avg_tokens_per_doc=avg_tokens_par_doc(token_count,document_count)
    #calcul le top k toekn 
    freq=freq_token(index)
    top_10_token=extract_top_token(freq,10)
    stemmed_tokens_snowball=[stemmer_snowball(token) for token in index.keys()]
    stemmed_index=update_json(stemmed_tokens_snowball,index)

    # Écrire l'index et les statistiques dans des fichiers JSON
    metadata= {
        'num_docs': document_count,
        'num_tokens': token_count,
        'avg_tokens_per_doc': avg_tokens_per_doc,
        'top_10_token':top_10_token
    }
    with open('metadata.json', 'w') as f:
        json.dump(metadata, f)


    with open("title.non_pos_index.json", "w") as f:
        json.dump(index,f)
    with open("mon_stemmer.title.non_pos_index.json", "w") as f:
        json.dump(stemmed_index,f)


    
if __name__=="__main__":
    run()